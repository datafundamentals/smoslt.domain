/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.domain module, one of many modules that belongs to smoslt

 smoslt.domain is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.domain is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.domain in the file smoslt.domain/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SideEffectBaseTest {
	private Zzz3_TesterGuy testGuy = new Zzz3_TesterGuy();
	
	@Before
	public void setUp(){
		
	}

	@Test
	public void testIsNameCompliant() {
		assertTrue("Isa_".matches(SideEffectBase.NAME_COMPLIANT_REGEX));
		assertTrue("Isa7_".matches(SideEffectBase.NAME_COMPLIANT_REGEX));
		assertTrue("Isa7_Foo".matches(SideEffectBase.NAME_COMPLIANT_REGEX));
		assertTrue(testGuy.isNameCompliant());
	}

	@Test
	public void testIsOrGroupName(){
		assertTrue("Isa0_Foo".matches(SideEffectBase.OR_GROUP_REGEX));
		assertFalse("Isa_Foo".matches(SideEffectBase.OR_GROUP_REGEX));
		assertTrue("Isa7_Foo".matches(SideEffectBase.OR_GROUP_REGEX));
		assertTrue(testGuy.isOrGroupMember());
	}
	@Test
	public void testIsOrGroupMember(){
		assertTrue("Isa7_Foo".matches(SideEffectBase.OR_GROUP_MEMBER_REGEX));
		assertFalse("Isa0_Foo".matches(SideEffectBase.OR_GROUP_MEMBER_REGEX));
		assertFalse("Isa_Foo".matches(SideEffectBase.OR_GROUP_MEMBER_REGEX));
		assertTrue(testGuy.isOrGroupMember());
	}
	
	@Test
	public void testIsOrGroupTitle(){
		assertTrue("Isa0_Foo".matches(SideEffectBase.OR_GROUP_TITLE_REGEX));
		assertFalse("Isa_Foo".matches(SideEffectBase.OR_GROUP_TITLE_REGEX));
		assertFalse("Isa7_Foo".matches(SideEffectBase.OR_GROUP_TITLE_REGEX));
		assertFalse(testGuy.isOrGroupTitle());
	}
	@Test
	public void testGetKey(){
		assertEquals("Zzz3",testGuy.getKey());
	}
	@Test
	public void testGetName(){
		assertEquals("TesterGuy",testGuy.getName());
	}

}
