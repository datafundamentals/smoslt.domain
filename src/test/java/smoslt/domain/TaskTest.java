/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.domain module, one of many modules that belongs to smoslt

 smoslt.domain is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.domain is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.domain in the file smoslt.domain/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class TaskTest {

	@Test
	public void testGetDurationDays() {
		int hours = 9;
		int hoursPerDay = 8;
		int days = hours / hoursPerDay;
		int remainder = hours % hoursPerDay;
		assertEquals(remainder, 1);
		if (remainder > 0) {
			days++;
		}
		assertEquals(2, days);

		Task task = new Task(9, 16, "foo", "bar");
		assertEquals(task.getDurationHours(), 9);
		assertEquals(task.getDurationDays(), 2);
		task = new Task(15, 16, "foo", "bar");
		assertEquals(task.getDurationHours(), 15);
		assertEquals(task.getDurationDays(), 2);
		task = new Task(1, 16, "foo", "bar");
		assertEquals(task.getDurationHours(), 1);
		assertEquals(task.getDurationDays(), 1);
		task = new Task(7, 16, "foo", "bar");
		assertEquals(task.getDurationHours(), 7);
		assertEquals(task.getDurationDays(), 1);
		task = new Task(8, 16, "foo", "bar");
		assertEquals(task.getDurationHours(), 8);
		assertEquals(task.getDurationDays(), 1);
		task = new Task(16, 16, "foo", "bar");
		assertEquals(task.getDurationHours(), 16);
		assertEquals(task.getDurationDays(), 2);
	}

}
