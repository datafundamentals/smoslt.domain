package smoslt.domain;

import java.util.ArrayList;
import java.util.List;

public class Models {
	// from http://en.wikipedia.org/wiki/Systems_development_life_cycle
	public static final String PHASE0_INITIATION = "Initiation";
	public static final String PHASE1_SYSTEM_CONCEPT_DEVELOPMENT = "System Concept Development";
	public static final String PHASE2_PLANNING = "Planning";
	public static final String PHASE3_REQUIREMENTS_ANALYSIS = "Requirements Analysis";
	public static final String PHASE4_DESIGN = "Design";
	public static final String PHASE5_DEVELOPMENT = "Development";
	public static final String PHASE6_INTEGRATION_TEST = "Integeration Testing";
	public static final String PHASE7_IMPLEMENTATION = "Implementation";
	public static final String PHASE8_OPERATIONS_MAINTENANCE = "Operations Maintenance";
	public static final String PHASE9_DISPOSITION = "Disposition";
	public static final String TASK_CRUD_CONTROLLER = " CRUD Controller";
	public static final String TASK_R_CRUD_MODEL = " RofCRUD Model";
	public static final String TASK_CU_CRUD_MODEL = " CUofCRUD Model";
	public static final String TASK_D_CRUD_MODEL = " DofCRUD Model";
	public static final String TASK_R_CRUD_VIEW = " RofCRUD View";
	public static final String TASK_CU_CRUD_VIEW = " CUofCRUD View";
	public static final String TASK_D_CRUD_VIEW = " DofCRUD View";
	public static final String TASK_CRUD_DOC = " CRUD Docs";
	public static final String TASK_CRUD_TEST = " CRUD Tests";
	public static final String TASK_CU_CRUD_API = " CUofCRUD API";
	public static final String TASK_R_CRUD_API = " RofCRUD API";
	public static final String TASK_D_CRUD_API = " DofCRUD API";
	public static final String TASK_CU_CRUD_API_TEMPLATE = "Template Code for  CUofCRUD API";
	public static final String TASK_R_CRUD_API_TEMPLATE = "Template Code for RofCRUD API";
	public static final String TASK_D_CRUD_API_TEMPLATE = "Template Code for  DofCRUD API";
	public static final String TASK_CU_CRUD_MSSG = " CUofCRUD Messaging";
	public static final String TASK_R_CRUD_MSSG = " RofCRUD Messaging";
	public static final String TASK_D_CRUD_MSSG = " DofCRUD Messaging";
	public static final String TASK_CU_CRUD_UI_DSGN = " CUofCRUD UI Design";
	public static final String TASK_R_CRUD_UI_DSGN = " RofCRUD UI Design";
	public static final String TASK_D_CRUD_UI_DSGN = " DofCRUD UI Design";
	public static final String TASK_CU_CRUD_DEV_DSGN = "CUofCRUD Dev Design";
	public static final String TASK_R_CRUD_DEV_DSGN = "RofCRUD Dev Design";
	public static final String TASK_D_CRUD_DEV_DSGN = "DofCRUD Dev Design";
	public static final String TASK_CU_CRUD_DATA_DSGN = "CUofCRUD Data Design";
	public static final String TASK_R_CRUD_DATA_DSGN = "RofCRUD Data Design";
	public static final String TASK_D_CRUD_DATA_DSGN = "DofCRUD Data Design";
	public static final String TASK_CU_CRUD_DATA_CUSTOMIZAITION = "CUofCRUD Data Customization";
	public static final String TASK_R_CRUD_DATA_CUSTOMIZAITION = "RofCRUD Data Customization";
	// from http://www.databaseanswers.org/data_models/
	// "Retail Orders",1
	// "Inventory",2
	// "Manufacturing",3
	// "Real Estate",4
	// "Education",5
	// "Insurance",6
	// "Human Resource Dept",7
	// "Social Media",8
	// "Logistics",9
	// "Medical",10
	// "my own custom domain"11
	public static final String MODEL_CUSTOM = "11";
	public static final String MODEL_MANUFACTURING = "3";
	public static final String MODEL_INVENTORY = "2";
	public static final String MODEL_RETAIL = "1";
	public static final String MODEL_REAL_ESTATE = "4";
	public static final String MODEL_EDUCATION = "5";
	public static final String MODEL_INSURANCE = "6";
	public static final String MODEL_SOCIAL_MEDIA = "8";
	public static final String MODEL_HR = "7";
	public static final String MODEL_LOGISTICS = "9";
	public static final String MODEL_MEDICAL = "10";
	public static final String MANUFACTURING_MODELS = "Suppliers\n"
			+ "SupplierType\n" + "MaintenanceContract\n" + "Staff\n"
			+ "Asset\n" + "AssetPart\n" + "FaultLog\n" + "Part\n"
			+ "FaultLogPart\n" + "PartFault\n" + "SkillType\n"
			+ "SkillRequired\n" + "Skill\n" + "EngineerVisit\n"
			+ "RefFaultStatus\n" + "EngineerSkill\n" + "StaffType\n"
			+ "AssetVendor\n" + "ShippingType\n" + "ShippingVendor\n"
			+ "Inventory\n" + "EngineerVisit\n" + "MaintenanceEngineer\n"
			+ "ProductHierarchy\n" + "ProductSupplier";
	public static final String INVENTORY_MODELS = "Supplier\n" + "Product\n"
			+ "ProductType\n" + "Sale\n" + "Shipment\n" + "InventoryLevel\n"
			+ "Calendar\n" + "Order\n" + "Warehouse\n" + "Address\n"
			+ "Customer\n" + "BinCode\n" + "Bin\n" + "Staff\n" + "Person\n"
			+ "Equipment\n" + "EquipmentType\n" + "Document\n" + "Shrinkage\n"
			+ "ShrinkageReport\n" + "Inquiry\n" + "Department\n" + "Demand\n"
			+ "DemandType\n" + "DemandStatus";
	public static final String RETAIL_MODELS = "Supplier\n" + "Product\n"
			+ "Order\n" + "OrderDetail\n" + "Employee\n"
			+ "EmployeeTerritory\n" + "Customer\n" + "Shipper\n"
			+ "CustomerDemo\n" + "Employee\n" + "Person\n" + "Region\n"
			+ "CustomerDemographic\n" + "Sale\n" + "RecordType\n" + "Tax\n"
			+ "TaxCode\n" + "Payment\n" + "Color\n" + "Size\n" + "Inventory\n"
			+ "Payment\n" + "PaymentType\n" + "Document\n" + "Inquiry";
	public static final String REAL_ESTATE_MODELS = "Supplier\n" + "Address\n"
			+ "Agent\n" + "Organization\n" + "Fee\n" + "Title\n" + "Tax\n"
			+ "TaxCode\n" + "Developer\n" + "Person\n" + "ContactInfo\n"
			+ "Investor\n" + "TaxRecord\n" + "Appraisal\n" + "Appraiser\n"
			+ "Invoice\n" + "InvoiceItem\n" + "Statement\n" + "Payment\n"
			+ "Demographic\n" + "Photos\n" + "PropertyType\n"
			+ "Encumberance\n" + "Legal\n" + "Document";
	public static final String EDUCATION_MODELS = "Student\n" + "StudentType\n"
			+ "Institution\n" + "InstitutionType\n" + "AttendanceRecord\n"
			+ "Event\n" + "Staff\n" + "Person\n" + "Address\n" + "Contract\n"
			+ "Activity\n" + "Assignment\n" + "Schedule\n" + "Exam\n"
			+ "ReportCard\n" + "ReportCardItem\n" + "Scholarship\n"
			+ "Transcript\n" + "CounselingRecord\n" + "Class\n" + "Teacher\n"
			+ "Registration\n" + "Subject\n" + "Location\n" + "Document";
	public static final String INSURANCE_MODELS = "Question\n"
			+ "QuestionType\n" + "Policy\n" + "PolicyCode\n"
			+ "GeographicalCode\n" + "Application\n" + "Customer\n"
			+ "Answer\n" + "Experience\n" + "ExperienceCode\n" + "Statute\n"
			+ "StatuteCode\n" + "Division\n" + "Person\n" + "Agent\n"
			+ "Contract\n" + "ContractType\n" + "Transaction\n" + "Payment\n"
			+ "CurrencyCode\n" + "Claim\n" + "Adjuster\n" + "Address\n"
			+ "Legal\n" + "ClaimType";
	public static final String SOCIAL_MEDIA_MODELS = "Medium\n" + "MediaType\n"
			+ "ApiSource\n" + "ApiSourceType\n" + "Relationship\n"
			+ "RelationshipType\n" + "Message\n" + "MessageType\n"
			+ "MessageProtocol\n" + "Identify\n" + "Source\n" + "MessageLog\n"
			+ "PrivacyPreference\n" + "Event\n" + "EventType\n"
			+ "Organization\n" + "OrganizationTransaction\n" + "Image\n"
			+ "Profile\n" + "ProfileType\n" + "Link\n" + "ContactRole\n"
			+ "Transaction\n" + "TransactionType\n" + "Fee";
	public static final String HR_MODELS = "Employee\n" + "Identity\n"
			+ "Ethnicity\n" + "Religion\n" + "PayGrade\n" + "Role\n"
			+ "Assignment\n" + "JobCode\n" + "Vacation\n"
			+ "PerformanceReview\n" + "Division\n" + "Supervisor\n" + "Event\n"
			+ "EventType\n" + "Accreditation\n" + "AccreditationType\n"
			+ "Benefit\n" + "BenefitType\n" + "LocalTax\n" + "LocalTaxType\n"
			+ "Schedule\n" + "ScheduleType\n" + "Agency\n" + "Contact\n"
			+ "SkillType";
	public static final String LOGISTICS_MODELS = "Suppliers\n" + "Role\n"
			+ "Incident\n" + "Organization\n" + "Document\n" + "BillLading\n"
			+ "Asset\n" + "Event\n" + "EventType\n" + "Customer\n"
			+ "Address\n" + "ShippingCode\n" + "ShippingLog\n" + "Message\n"
			+ "MessageType\n" + "ProductShipment\n" + "LocationType\n"
			+ "Inquiry\n" + "InquiryType\n" + "TaxCode\n" + "Tax\n"
			+ "CustomsCode\n" + "Status\n" + "ExperienceRating\n" + "Bribe";
	public static final String MEDICAL_MODELS = "CommunityMedicalCenter\n"
			+ "Patient\n" + "RefApptLocation\n" + "Address\n"
			+ "RefDiscipline\n" + "ProfessionalStaff\n" + "Appointment\n"
			+ "AppointmentStaff\n" + "Person\n" + "Medication\n" + "Supplier\n"
			+ "SupplierType\n" + "Diagnosis\n" + "Donor\n" + "DonorType\n"
			+ "Religion\n" + "Ethnicity\n" + "StaffRole\n" + "PatientRecord\n"
			+ "PatientRecordType\n" + "AppointentStatusCode\n" + "Service\n"
			+ "ServiceCode\n" + "Disability\n" + "DisabilityType";

	public static final String PHASE_PROCESS_MODELS = PHASE0_INITIATION + "\n"
			+ PHASE1_SYSTEM_CONCEPT_DEVELOPMENT + "\n" + PHASE2_PLANNING + "\n"
			+ PHASE3_REQUIREMENTS_ANALYSIS + "\n" + PHASE4_DESIGN + "\n"
			+ PHASE5_DEVELOPMENT + "\n" + PHASE6_INTEGRATION_TEST + "\n"
			+ PHASE7_IMPLEMENTATION + "\n" + PHASE8_OPERATIONS_MAINTENANCE
			+ "\n" + PHASE9_DISPOSITION;
	public static final String RESOURCE_PROJ_MGR = "ProjMgr";
	public static final String RESOURCE_OPS = "Ops";
	public static final String RESOURCE_DEV_IMPL = "DevImpl";
	public static final String RESOURCE_DEV_DESIGN = "DevDesign";
	public static final String RESOURCE_QA_TEST = "QaTest";
	public static final String RESOURCE_DBA = "Dba";
	public static final String RESOURCE_BUS_ANLST = "BusAnlst";
	public static final String RESOURCE_ROLES = RESOURCE_BUS_ANLST + " "
			+ RESOURCE_DBA + " " + RESOURCE_DEV_DESIGN + " "
			+ RESOURCE_DEV_IMPL + " " + RESOURCE_OPS + " " + RESOURCE_PROJ_MGR
			+ " " + RESOURCE_QA_TEST;
	private static List<String> phases;

	public static List<String> getPhases() {
		if (phases == null) {
			phases = new ArrayList<String>();
			phases.add(PHASE0_INITIATION);
			phases.add(PHASE1_SYSTEM_CONCEPT_DEVELOPMENT);
			phases.add(PHASE2_PLANNING);
			phases.add(PHASE3_REQUIREMENTS_ANALYSIS);
			phases.add(PHASE4_DESIGN);
			phases.add(PHASE5_DEVELOPMENT);
			phases.add(PHASE6_INTEGRATION_TEST);
			phases.add(PHASE7_IMPLEMENTATION);
			phases.add(PHASE8_OPERATIONS_MAINTENANCE);
			phases.add(PHASE9_DISPOSITION);
		}
		return phases;
	}
}
