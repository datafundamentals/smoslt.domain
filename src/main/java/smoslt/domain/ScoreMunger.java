package smoslt.domain;

public class ScoreMunger {
	private ScoreKey scoreKey;
	private int scoreLimit;
	private int scoreWeight;

	public ScoreMunger(String scoreKey, int scoreWeight, int scoreLimit) {
		this.scoreKey = ScoreKey.get(scoreKey);
		this.scoreWeight = scoreWeight;
		this.scoreLimit = scoreLimit;
	}

	public ScoreKey getScoreKey() {
		return scoreKey;
	}

	public int getScoreWeight() {
		return scoreWeight;
	}

	public int getScoreLimit() {
		return scoreLimit;
	}
}
