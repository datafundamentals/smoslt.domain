/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.domain module, one of many modules that belongs to smoslt

 smoslt.domain is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.domain is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.domain in the file smoslt.domain/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.domain;

public enum ScoreKey {
    FAIL,
	Hours,
    ScalesUp,
    FeedbackSpeed,
    ManagerSpeak, 
    POLR, // path of least resistance
    LongTerm, // How long it takes to 
    RDD // Resume Driven Development, or exciting for developers
//    Cost,
//    TimeToMarket
//    Usability,
//    Durability,
//    UbiquitySkill, // ability to easily hire replacements/growth
//    ScalesDown, // two way scaling
//    CAP, // Consistency, cap theorem
//    Risk
    ;  
	public static synchronized ScoreKey get(String scoreKey) {
		ScoreKey returnValue = null;
		for (int i = 0; i < values().length; i++) {
			if (null != scoreKey && scoreKey.equals(values()[i].toString())) {
				returnValue = values()[i];
				break;
			}
		}
		if(returnValue==null){
			throw new IllegalArgumentException("ScoreKey enum named '"+scoreKey+ "' is not a valid ScoreKey enum");
		}
		return returnValue;
	}  
}
