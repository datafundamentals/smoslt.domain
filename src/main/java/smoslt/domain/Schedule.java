/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.domain module, one of many modules that belongs to smoslt

 smoslt.domain is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.domain is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.domain in the file smoslt.domain/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Schedule implements Serializable {
	private static final long serialVersionUID = 8801308795488867821L;
	private Set<Resource> resources;
	private List<Task> tasks;
	private TeamTaskSet teamTaskSet = new TeamTaskSet();
	private Map<String, List<Resource>> resourceGroups = new LinkedHashMap<String, List<Resource>>();
	private Map<Long, Set<Long>> assignments = new LinkedHashMap<Long, Set<Long>>();
	private ResourceTracker resourceTracker;
	private ProjectProfile projectProfile;
	
	public TeamTaskSet getTeamTaskSet() {
		return teamTaskSet;
	}

	private Schedule() {
	}

	public Schedule(ScheduleBuild build) {
		this.tasks = build.getTasks();
		buildTeamTaskSet();
		this.resources = build.getResources();
		buildResourceGroups();
		resourceTracker = new ResourceTracker(resources);
	}

	public Set<Resource> getResources() {
		return resources;
	}

	public List<Task> getTaskList() {
		if (tasks == null) {
			throw new RuntimeException();
		}
		return tasks;
	}

	public ResourceTracker getResourceTracker() {
		return resourceTracker;
	}

	public Resource getResource(long resourceId) {
		Resource chosenResource = null;
		for (Resource resource : resources) {
			if (resource.getId() == resourceId) {
				chosenResource = resource;
				break;
			}
		}
		return chosenResource;
	}

	public ProjectProfile getProjectProfile() {
		return projectProfile;
	}

	public void setProjectProfile(ProjectProfile projectProfile) {
		this.projectProfile = projectProfile;
	}

	private void buildTeamTaskSet() {
		for (Task task : tasks) {
			teamTaskSet.getTaskSetId(task);
		}
	}

	private void buildResourceGroups() {
		if (null != resources) {
			for (Resource resource : resources) {
				String resourceGroupName = resource.getGroup();
				if (resourceGroupName != null
						&& resourceGroupName.trim().length() > 0) {
					if (!resourceGroups.keySet().contains(resourceGroupName)) {
						resourceGroups.put(resourceGroupName,
								new ArrayList<Resource>());
					}
				}
				// at this point all we have done is added the group names from
				// the
				// resources themselves, to the resourceGroups map. This will
				// act as
				// a check against any bogus group names that come in from
				// tasks,
				// since we are relying on them being one and the same
			}
			for (Task task : tasks) {
				for (String resourceSpecification : task
						.getResourceSpecifications()) {
					parseResourceSpecification(task, resourceSpecification);
				}
			}
		}else{
			throw new RuntimeException("gotta have resources");
		}
	}

	void parseResourceSpecification(Task task, String resourceSpecification) {
		if (isGroupSpecification(resourceSpecification)) {
			parseGroupSpecification(task, resourceSpecification);
		} else if (isValidNamedResource(resourceSpecification)) {
			parseNamedResource(task, resourceSpecification);
		} else {
			String message = "Invalid resourceSpecification: '"
					+ resourceSpecification
					+ "' was either not a validated named resource, or an int and valiated named group";
			throw new RuntimeException(message);
		}
	}

	/**
	 * adds named resource by firing task.addSpecifiedResource()
	 * 
	 * @param resourceSpecification
	 */
	void parseNamedResource(Task task, String resourceSpecification) {
		for (Resource resource : resources) {
			if (resourceSpecification.equalsIgnoreCase(resource.getName())) {
				task.addSpecifiedResource(resource);
			}
		}
	}

	/**
	 * 
	 * @param resourceSpecification
	 * @return true if the named resource can be found in the resourceGroups
	 */

	boolean isValidNamedResource(String resourceSpecification) {
		for (Resource resource : resources) {
			if (resourceSpecification.equalsIgnoreCase(resource.getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Fires task.addGroupRequirement() from parsed resourceSpecification
	 * 
	 * @param resourceSpecification
	 */
	void parseGroupSpecification(Task task, String resourceSpecification) {
		String potentialInt = resourceSpecification.substring(0,
				resourceSpecification.indexOf(" "));
		int count = Integer.parseInt(potentialInt);
		String group = resourceSpecification.substring(
				resourceSpecification.indexOf(" "),
				resourceSpecification.length()).trim();
		task.addGroupRequirement(group, count);
	}

	/**
	 * 
	 * @param resourceSpecification
	 * @return true if it parses as an int, a space, then a group name which is
	 *         recognized as one of the resourceGroups
	 */
	boolean isGroupSpecification(String resourceSpecification) {
		boolean isGroupSpecification = false;
		if (resourceSpecification.contains(" ")) {
			String potentialInt = resourceSpecification.substring(0,
					resourceSpecification.indexOf(" "));
			try {
				int count = Integer.parseInt(potentialInt);
				if (count > 0) {
					String potentialGroup = resourceSpecification.substring(
							resourceSpecification.indexOf(" "),
							resourceSpecification.length()).trim();
					if (resourceGroups.keySet().contains(potentialGroup)) {
						isGroupSpecification = true;
					}
				}
			} catch (NumberFormatException e) {
				// expected, not a problem - false is returned
			}
		}
		return isGroupSpecification;
	}

	public Map<Long, Set<Long>> getAssignments() {
		return assignments;
	}


	public void addAssignment(long taskId, long resourceId) {
		Set<Long> taskAssignment = assignments.get(taskId);
		if (taskAssignment == null) {
			taskAssignment = new HashSet<Long>();
			assignments.put(taskId, taskAssignment);
		}
		taskAssignment.add(resourceId);
	}

	public int getCosts() {
		int costs = 0;
		Iterator<Set<Integer>> persons = resourceTracker.getResourceUsedPerDay().values().iterator();
		while(persons.hasNext()){
			Iterator<Integer> days = persons.next().iterator();
			while(days.hasNext()){
				days.next();
				costs++;
			}
		}
		return costs;
	}

	
}
