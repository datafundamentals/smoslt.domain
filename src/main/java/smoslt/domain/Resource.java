/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.domain module, one of many modules that belongs to smoslt

 smoslt.domain is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.domain is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.domain in the file smoslt.domain/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.domain;

import java.io.Serializable;
import java.util.UUID;

/**
 * Answers the question "What group is this resource in?"
 * 
 * @author petecarapetyan
 *
 */
public class Resource implements Serializable {
	private static final long serialVersionUID = -2611171007547943793L;
	private long id;
	private String name;
	private String group;

	public Resource(String name, String group) {
		this.id = (long) UUID.randomUUID().toString().hashCode();
		this.name = name;
		this.group = group;
	}

	public Resource(long id, String name, String group) {
		this.id = id;
		this.name = name;
		this.group = group;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getGroup() {
		return group;
	}

	public int firstDayDoubleScheduled() {
		return 0;
	}

	public int daysIdleBeforeThis() {
		return 0;
	}

	public boolean isGroupSpecifierOnly() {
		boolean isGroupSpecifierOnly = false;
		if (name.contains(" ")) {
			String firstName = name.trim().substring(0, name.indexOf(" "));
			String lastName = name.substring(name.indexOf(" "), name.length());
			int trialValue = -1;
			try {
				trialValue = Integer.valueOf(firstName);
			} catch (NumberFormatException e) {
				// expected more often than not
			}
			if (!group.equals(lastName) && trialValue > 0) {
				isGroupSpecifierOnly = true;
			}else if (group.equals(lastName) && trialValue == -1) {
				throw new IllegalStateException("Group named '"+ group + "' doesn't seem right");
			}		
		}
		return isGroupSpecifierOnly;
	}
}
