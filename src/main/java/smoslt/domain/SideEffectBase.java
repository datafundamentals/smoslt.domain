/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.domain module, one of many modules that belongs to smoslt

 smoslt.domain is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.domain is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.domain in the file smoslt.domain/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.domain;

import java.util.HashMap;
import java.util.Map;

import javax.management.OperationsException;

public abstract class SideEffectBase implements SideEffect {
	
	final static String NAME_COMPLIANT_REGEX = "^[A-Z][a-z][a-z][0-9]?_.*";
	final static String OR_GROUP_REGEX = "^[A-Z][a-z][a-z][0-9]_.*";
	final static String OR_GROUP_TITLE_REGEX = "^[A-Z][a-z][a-z]0_.*";
	final static String OR_GROUP_MEMBER_REGEX = "^[A-Z][a-z][a-z][1-9]_.*";
	private static Map<ScoreKey, Integer> saturationScoreAllocationPercentageMap = new HashMap<ScoreKey, Integer>();
	private static Map<ScoreKey, Integer> saturationLimitRemainingMap = new HashMap<ScoreKey, Integer>();
	private static Map<String, Boolean> initializationMap = new HashMap<String, Boolean>();
	protected static int MAX = 100;
	
	protected boolean isFirstTaskWith(String workRole){
		if(initializationMap.containsKey(workRole)&&!initializationMap.get(workRole)){
			return false;
		}else{
			initializationMap.put(workRole, false);
			return true;
		}
	}

	@Override
	public boolean isNameCompliant() {
		return this.getClass().getSimpleName().matches(NAME_COMPLIANT_REGEX);
	}

	@Override
	public boolean isOrGroupName() {
		return this.getClass().getSimpleName().matches(OR_GROUP_REGEX);
	}

	@Override
	public boolean isOrGroupMember() {
		return this.getClass().getSimpleName().matches(OR_GROUP_MEMBER_REGEX);
	}

	@Override
	public boolean isOrGroupTitle() {
		return this.getClass().getSimpleName().matches(OR_GROUP_TITLE_REGEX);
	}

	@Override
	public String getKey() {
		checkFirst();
		return this.getClass().getSimpleName()
				.substring(0, this.getClass().getSimpleName().indexOf("_"));
	}

	@Override
	public String getGroupKey() {
		checkFirst();
		String name = null;
		if (isOrGroupName()) {
			name = this
					.getClass()
					.getSimpleName()
					.substring(0,
							this.getClass().getSimpleName().indexOf("_") - 1);
		}
		return name;
	}

	@Override
	public String getName() {
		checkFirst();
		return this
				.getClass()
				.getSimpleName()
				.substring(this.getClass().getSimpleName().indexOf("_") + 1,
						this.getClass().getSimpleName().length());
	}

	@Override
	public boolean isSaturationScored() {
		if (saturationScoreAllocationPercentageMap.keySet().contains(getKey())) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void initializeSaturationLimit(ScoreKey scoreKey, int limit) {
		if (limit < 1) {
			throw new IllegalArgumentException(
					"limit must be set as greater than 0");
		}
		if (!saturationScoreAllocationPercentageMap.containsKey(scoreKey)) {
			saturationScoreAllocationPercentageMap.put(scoreKey, limit);
			saturationLimitRemainingMap.put(scoreKey, limit);
		}
	}

	@Override
	public void incrementSaturationLimit(ScoreKey scoreKey, int limit) {
		if (!saturationScoreAllocationPercentageMap.keySet().contains(scoreKey)) {
			throw new IllegalAccessError(
					"setSaturationLimit() must be called prior to incrementSaturationLimit()");
		}
		int limitToIncrement = limit < getSaturationLimitRemaining(scoreKey) ? limit
				: getSaturationLimitRemaining(scoreKey);
		int newLimit = (byte) (saturationScoreAllocationPercentageMap
				.get(scoreKey) + limitToIncrement);
		saturationScoreAllocationPercentageMap.put(scoreKey, newLimit);
	}

	@Override
	public int getSaturationLimitRemaining(ScoreKey scoreKey) {
		if (saturationLimitRemainingMap.keySet().contains(scoreKey)) {
			return saturationLimitRemainingMap.get(scoreKey);
		} else {
			throw new IllegalAccessError(
					"Cannot ask for remaining limit on limit that has never been set");
		}
	}

	@Override
	public int getSaturationLimit(ScoreKey scoreKey) {
		if (saturationScoreAllocationPercentageMap.keySet().contains(scoreKey)) {
			return saturationScoreAllocationPercentageMap.get(scoreKey);
		} else {
			return 0;
		}
	}

	void checkFirst() {
		if (!this.isNameCompliant()) {
			throw new RuntimeException(
					"Classname for a sideEffect must be name compliant, such as starting with Abc_, but you tried to use "
							+ this.getClass().getSimpleName());
		}
	}

}
