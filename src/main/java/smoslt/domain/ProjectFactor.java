package smoslt.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProjectFactor implements Serializable {
	private String id;
	private String category;
	private String name;
	private String description;
	private List<String> choices = new ArrayList<String>();
	private int choice;
	private String value;
	private int bottomRange;
	private int topRange;
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public ProjectFactor() {
	}

	public ProjectFactor(String id, String name, String description,
			String[] choices, int choice) {
		this.id = id;
		this.name = name;
		this.description = description;
		getChoices(choices);
		this.choice = choice;
	}

	public ProjectFactor(String id, String name, String description,
			int choice, int bottomRange, int topRange) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.choice = choice;
	}

	public ProjectFactor(String id, String name, String description,
			String value) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getChoices() {
		return choices;
	}

	public void setChoices(List<String> choices) {
		this.choices = choices;
	}

	public int getChoice() {
		return choice;
	}

	public void setChoice(int choice) {
		this.choice = choice;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getBottomRange() {
		return bottomRange;
	}

	public void setBottomRange(int bottomRange) {
		this.bottomRange = bottomRange;
	}

	public int getTopRange() {
		return topRange;
	}

	public void setTopRange(int topRange) {
		this.topRange = topRange;
	}

	private void getChoices(String[] theseChoices) {
		for (String aChoice : theseChoices) {
			this.choices.add(aChoice);
		}
	}

	public ProjectFactorType deduceProjectFactorType() {
		if (null != this.choices && !this.choices.isEmpty() && topRange != 0) {
			throw new IllegalStateException("Cannot be both "
					+ ProjectFactorType.MultipleChoice + " and "
					+ ProjectFactorType.Slider);
		} else if (null != this.choices && !this.choices.isEmpty()) {
			return ProjectFactorType.MultipleChoice;
		} else if (null != this.text && !this.text.isEmpty()) {
			return ProjectFactorType.TextArea;
		} else if (topRange > 2 && topRange - bottomRange > 3) {
			return ProjectFactorType.Slider;
		} else {
			return ProjectFactorType.TextField;
		}
	}

}
