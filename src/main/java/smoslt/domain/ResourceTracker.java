/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.domain module, one of many modules that belongs to smoslt

 smoslt.domain is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.domain is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.domain in the file smoslt.domain/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Singleton for maintaining the sum total of all reserved days for all
 * Resources of an entie Schedule run.
 * 
 * For example if Jim was scheduled so far on days 1-3, and day 6, this class
 * would be where that information was maintained.
 * 
 * @author petecarapetyan
 *
 */
public class ResourceTracker  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5261216946131881706L;
	private Map<Long, Set<Integer>> resourceUsedPerDay = new HashMap<Long, Set<Integer>>();

	public ResourceTracker(Set<Resource> resources) {
		for (Resource resource : resources) {
			resourceUsedPerDay.put(resource.getId(), new TreeSet<Integer>());
		}
	}

	public Map<Long, Set<Integer>> getResourceUsedPerDay() {
		return resourceUsedPerDay;
	}

	public boolean reserveDay(Resource resource, int day, long taskId) {
		//TODO memory leak here somewhere 
		boolean reserveDay = false;
//		System.out.println("RESERVING DAY "+ day + " FOR TASK " + taskId);
		Set<Integer> reservedDayStash = resourceUsedPerDay
				.get(resource.getId());
		if (null != reservedDayStash
				&& !reservedDayStash.contains(new Integer(day))) {
			reservedDayStash.add(new Integer(day));
			reserveDay = true;
		} else if (reservedDayStash.contains(new Integer(day))) {
			System.err.println("Attempted to add startDay "
					+ day + " a second time to the ResourceTracker for resource "+resource.getName() +", must "
					+ "always check first and never add the startDay twice");
		}
		return reserveDay;
	}

	public int getLastReservedDay(Resource resource) {
		Set<Integer> reservedDayStash = resourceUsedPerDay
				.get(resource.getId());
		int highestDay = 0;
		if (null != reservedDayStash) {
			for (int day : reservedDayStash) {
				if (day > highestDay) {
					highestDay = day;
				}
			}
		}
		return highestDay;
	}

	public int getUnreservedDaysImmediatelyPreceding(Resource resource, int day) {
		int returnValue = 0;
		if (day > 1) {
			int dayToTest = day - 1;
			int previousDay = day;
			while (previousDay - dayToTest == 1) {
				previousDay = dayToTest;
				if (!isDayReserved(resource, dayToTest)) {
					returnValue++;
					dayToTest--;
				}
				// can never be less than the first day
				if (dayToTest < 1) {
					break;
				}
			}
		}
		return returnValue;
	}

	public boolean isDayReserved(Resource resource, int day) {
		boolean isDayReserved = false;
		Set<Integer> reservedDayStash = getResourceUsedPerDay().get(
				resource.getId());
		if (reservedDayStash != null && reservedDayStash.size() > 0) {
			if (reservedDayStash.contains(day)) {
				isDayReserved = true;
			}
		}
		return isDayReserved;
	}

	/*
	 * Little too complex for my tastes but have not yet developed a simpler
	 * approach
	 */
	public SortedMap<Integer, Integer> durationsOfUnreservedDaysLargeEnough(
			int[] daysNotReserved, int minimumNumberDays) {
		SortedMap<Integer, Integer> firstDayToDuration = new TreeMap<Integer, Integer>();
		int dayToCompare = 0;
		int previousDay = 0;
		int currentRecordedDuration = 0;
		int firstDay = 1;
		for (int i = 0; i < daysNotReserved.length; i++) {
			dayToCompare = daysNotReserved[i];
			if (dayToCompare - previousDay == 1) {
				currentRecordedDuration++;
			} else {
				if (currentRecordedDuration >= minimumNumberDays) {
					firstDayToDuration.put(firstDay, currentRecordedDuration);
				}
				firstDay = dayToCompare;
				currentRecordedDuration = 1;
			}
			previousDay = dayToCompare;
		}
		if (currentRecordedDuration >= minimumNumberDays) {
			firstDayToDuration.put(firstDay, currentRecordedDuration);
		}
		return firstDayToDuration;
	}
}
