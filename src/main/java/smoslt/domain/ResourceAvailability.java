/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.domain module, one of many modules that belongs to smoslt

 smoslt.domain is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.domain is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.domain in the file smoslt.domain/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.domain;

import java.io.Serializable;

/**
 * Answers the questions
 * "Does this resource work for ?"
 * 
 * Encapsulates a single gap in a resources schedule. If a resource was scheduled Monday
 * and then again on Friday but had a gap in-between that was not committed to
 * any specific task, that gap would become a single ResourceAvailability.
 * 
 * This availability has a possibly confusing feature in that it constructs
 * itself from a specific date forward. So if it was constructed on a Wednesday
 * in the middle of a week long Mon-Fri schedule gap, it would describe the gap
 * as 3 days, plus 2 unUsedDaysPreceding.
 * 
 * Additionally, if it was queried with
 * "Hey I need 2 days of availability starting Wednesday" but the entire week
 * was available, it would describe the gap as 2 days beginning Wed, but also
 * having 2 unUsedDaysPreceding, and 1 unUsedDayAfter
 * 
 * The reason for the unUsedDaysPreceeding and unUsedDaysAfter calibration is
 * less than obvious until you look at the decision of picking which team member
 * to schedule for a specific task. If two differently scheduled team members
 * both had a two day gap, but one had no unUsedDaysPreceding and
 * unUsedDaysAfter, then you might wish to select between the two based on that.
 * Either by selecting the one with tightest gap, to leave the looser gap to a
 * better suited gap utilization, or alternately scheduling the one with the
 * looser gap, if the schedule designer was more interested in keeping slack in
 * the schedule.
 *
 */
public class ResourceAvailability  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1938700111942113436L;
	public final static int A_LOT = 10000;
	Resource resource;
	private int startDay;
	private int daysAvailable;
	private int unUsedDaysPreceding;
	private int unUsedDaysAfter;

	public ResourceAvailability(Resource resource, int startDay,
			int daysAvailable, int unUsedDaysPreceding, int unUsedDaysAfter) {
		this.resource = resource;
		this.startDay = startDay;
		this.daysAvailable = daysAvailable;
		this.unUsedDaysPreceding = unUsedDaysPreceding;
		this.unUsedDaysAfter = unUsedDaysAfter;
	}

	public boolean scheduleWorks(int thisStartDay, int duration) {
		boolean scheduleWorks = false;
		if (thisStartDay >= (thisStartDay - unUsedDaysPreceding)
				&& (thisStartDay + duration) <= (thisStartDay + daysAvailable + unUsedDaysAfter)) {
			scheduleWorks = true;
		}
		return scheduleWorks;
	}

	public Resource getResource() {
		return resource;
	}

	public int getFirstDayAvailable() {
		return startDay;
	}

	public int getUnUsedDaysPreceding() {
		return unUsedDaysPreceding;
	}

	public int getUnUsedDaysAfter() {
		return unUsedDaysAfter;
	}

	public int getDaysDuration() {
		return daysAvailable;
	}

}
