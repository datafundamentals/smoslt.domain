/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.domain module, one of many modules that belongs to smoslt

 smoslt.domain is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.domain is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.domain in the file smoslt.domain/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.domain;

import java.io.Serializable;


public interface SideEffect extends Serializable{
	void go(ScoreTracker scoreTracker, Task task);	
	public boolean isNameCompliant();
	public boolean isOrGroupName();
	public boolean isOrGroupMember();
	public boolean isOrGroupTitle();
	public String getKey();
	public String getGroupKey();
	public String getName();
	public boolean isSaturationScored();
	public void initializeSaturationLimit(ScoreKey scoreKey, int limit);
	public void incrementSaturationLimit(ScoreKey scoreKey, int limit);
	public int getSaturationLimitRemaining(ScoreKey scoreKey);
	public int getSaturationLimit(ScoreKey scoreKey);
}
