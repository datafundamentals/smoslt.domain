/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.domain module, one of many modules that belongs to smoslt

 smoslt.domain is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.domain is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.domain in the file smoslt.domain/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;

/**
 * Answers this question "Who needs to do this work, how much time needs to be
 * allocated, and what other Tasks have to be done before this work can start?"
 * 
 * "Who" in this case can be either a member of a group, or a specific, named
 * individual.
 * 
 * @author petecarapetyan
 *
 */
public class Task implements Serializable {
	private static final long serialVersionUID = 683552385554970295L;
	private int startDay;
	private int durationHours;
	private long id;
	private String name;
	private String resourceSpecifications;
	private List<Task> predecessors;
	private List<Resource> specifiedResources;
	private Map<String, Integer> groupRequirements = new LinkedHashMap<String, Integer>();

	private Task() {
	}

//	public Task(int durationDays, int startDay, long id, String name, String resourceSpecifications) {
//		System.out.println("SETTING START DAY " + startDay);
//		this.id = id;				
//		this.durationDays = durationDays;
//		this.name = name;
//		this.resourceSpecifications = resourceSpecifications;
//		this.startDay = startDay;
//	}
	
	public Task(int durationHours, int startDay, String name, String resourceSpecifications) {
//		System.out.println("SETTING START DAY " + startDay);
		this.id = (long)UUID.randomUUID().toString().hashCode();				
		this.durationHours = durationHours;
		this.name = name;
		this.resourceSpecifications = resourceSpecifications;
		this.startDay = startDay;
	}
	
	public String getWorkRole(){
		if(name.contains(":")){
			return name.substring(0, name.indexOf(":"));
		}else{
			return null;
		}
	}

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public int getDurationDays() {
		int days = durationHours / 8;
		if ((durationHours % 8) > 0) {
			days++;
		}
		return days;
	}


	public int getDurationHours() {
		return durationHours;
	}

	public List<String> getResourceSpecifications() {
		List<String> resourceSpecs = new ArrayList<String>();
		if (resourceSpecifications != null
				&& resourceSpecifications.trim().length() > 0) {
			StringTokenizer stk = new StringTokenizer(resourceSpecifications,
					",");
			while (stk.hasMoreTokens()) {
				String token = stk.nextToken().trim();
				resourceSpecs.add(token);
			}
		}
		return resourceSpecs;
	}

	public List<Task> getPredecessors() {
		return predecessors;
	}

	public List<Resource> getSpecifiedResources() {
		return specifiedResources;
	}

	public void addSpecifiedResource(Resource resource) {
		if (null == specifiedResources) {
			specifiedResources = new ArrayList<Resource>();
		}
		specifiedResources.add(resource);
	}

	public void addPredecessor(Task task) {
		if (predecessors == null) {
			predecessors = new ArrayList<Task>();
		}
		predecessors.add(task);
	}

	public String getName() {
		return name;
	}

	public void addGroupRequirement(String group, int quantityRequired) {
		groupRequirements.put(group, quantityRequired);
	}

	public Map<String, Integer> getGroupRequirements() {
		return groupRequirements;
	}


	public void setStartDay(int startDay) {
		this.startDay = startDay;
	}

	public void modifyDuration(double factor) {
		this.durationHours = (int)(durationHours*factor);
	}

	public int getStartDay() {
		return startDay;
	}

	public void addDuration(double addedDuration) {
		durationHours = durationHours + (int)addedDuration;
	}

}
