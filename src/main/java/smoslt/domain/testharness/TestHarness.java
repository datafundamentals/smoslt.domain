package smoslt.domain.testharness;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import smoslt.domain.Resource;
import smoslt.domain.Schedule;
import smoslt.domain.ScheduleBuild;
import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreTracker;
import smoslt.domain.SideEffect;
import smoslt.domain.Task;

public class TestHarness {
	Task task1 = new Task(16, 1, "DevImpl: The First Team Task", "2 DevImpl");
	Task task2 = new Task(8, 1, "DevImpl: A One Person Task", "1 DevImpl");
	Task task3 = new Task(3, 1, "DevImpl: B One Person Task", "1 DevImpl");
	List<Task> tasks = new ArrayList<Task>();
	Resource resource1 = new Resource("1 DevImpl", "DevImpl");
	Resource resource2 = new Resource("2 DevImpl", "DevImpl");
	Resource resource3 = new Resource("John Smith", "DevImpl");
	Resource resource4 = new Resource("Mary Williams", "DevImpl");
	Set<Resource> resources = new HashSet<Resource>();
	ScheduleBuild scheduleBuild;
	Schedule schedule;
	ScoreTracker scoreTracker;

	List<SideEffect> sideEffects = new ArrayList<SideEffect>();

	public TestHarness() {
		tasks.add(task1);
		tasks.add(task2);
		tasks.add(task3);
		resources.add(resource1);
		resources.add(resource2);
		resources.add(resource3);
		resources.add(resource4);
		scheduleBuild = getScheduleBuild();
		schedule = new Schedule(scheduleBuild);
		initializeScoreTracker();
	}

	private void initializeScoreTracker() {
		scoreTracker = new ScoreTracker() {

			@Override
			public void increment(ScoreKey scoreKey, int increment) {
			}

			@Override
			public List<Integer> getScores() {
				return new ArrayList<Integer>();
			}

			@Override
			public int getScore(ScoreKey key) {
				return 0;
			}

			@Override
			public Schedule getSchedule() {
				return schedule;
			}

			@Override
			public List<ScoreKey> getHeaders() {
				return new ArrayList<ScoreKey>();
			}
		};
	}

	private ScheduleBuild getScheduleBuild() {
		scheduleBuild = new ScheduleBuild() {

			@Override
			public List<Task> getTasks() {
				return tasks;
			}

			@Override
			public Set<Resource> getResources() {
				return resources;
			}
		};
		return scheduleBuild;
	}


	public List<Task> getTasks() {
		return tasks;
	}


	public Set<Resource> getResources() {
		return resources;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public List<SideEffect> getSideEffects() {
		return sideEffects;
	}

	public void setScheduleBuild(ScheduleBuild scheduleBuild) {
		this.scheduleBuild = scheduleBuild;
	}

	public void setScoreTracker(ScoreTracker scoreTracker) {
		this.scoreTracker = scoreTracker;
	}

	public ScoreTracker getScoreTracker() {
		return scoreTracker;
	}
}
