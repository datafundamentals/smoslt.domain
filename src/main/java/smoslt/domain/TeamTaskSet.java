/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.domain module, one of many modules that belongs to smoslt

 smoslt.domain is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.domain is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.domain in the file smoslt.domain/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * A team task set is a sequence of tasks which would hope to share the same
 * team of resources and a contiguous sequence.
 * 
 * By implication, there is a natural assumption that this sequence of tasks
 * works best when the same team sticks with the whole sequence as a single,
 * uninterrupted flow of work.
 * 
 * As an object, TeamTaskSet is merely a wrapper around a Map<String,
 * List<Task>> with a few convenience methods for accessing data within that Map
 *
 */
public class TeamTaskSet  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9056384413250250723L;
	private Map<Long, List<Task>> teamTaskSet = new LinkedHashMap<Long, List<Task>>();

	public long getTaskSetId(Task task) {
		long returnKey = 0;
		returnKey = findExistingTaskId(task);
		if (0 == returnKey) {
			returnKey = install(task);
		}
		return returnKey;
	}

	private long install(Task task) {
		long returnKey = 0;
		if (task.getPredecessors() == null || task.getPredecessors().isEmpty()) {
			List<Task> tasks = new ArrayList<Task>();
			tasks.add(task);
			teamTaskSet.put(task.getId(), tasks);
			returnKey = task.getId();
		} else {
			Task firstPredecessor = task.getPredecessors().get(0);
			returnKey = installOrFind(firstPredecessor);
			teamTaskSet.get(returnKey).add(task);
		}
		return returnKey;
	}

	private long installOrFind(Task task) {
		long returnKey = findExistingTaskId(task);
		if (0 == returnKey) {
			returnKey = install(task);
		}
		return returnKey;
	}

	private long findExistingTaskId(Task task) {
		// first, check to see if it is already a root task
		long returnKey = findExistingRootTaskId(task);
		// if still null, look in all tasks
		if (0 == returnKey) {
			for (long key : teamTaskSet.keySet()) {
				List<Task> tasks = teamTaskSet.get(key);
				for (Task childTask : tasks) {
					if (task.getId()==childTask.getId()) {
						returnKey = key;
						break;
					}
				}
				// break out if you already found it
				if (0 != returnKey) {
					break;
				}
			}
		}
		return returnKey;
	}

	private long findExistingRootTaskId(Task task) {
		for (long key : teamTaskSet.keySet()) {
			if (task.getId()==key) {
				return key;
			}
		}
		return 0L;
	}

	public List<Long> getRootTasksIds() {
		List<Long> rootTasks = new ArrayList<Long>();
		for (long taskId : teamTaskSet.keySet()) {
			rootTasks.add(taskId);
		}
		return rootTasks;
	}

	public List<Task> getTeamTaskList(long rootTaskId) {
		return teamTaskSet.get(rootTaskId);
	}

}
