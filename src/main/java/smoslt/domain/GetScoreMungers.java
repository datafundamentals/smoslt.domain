package smoslt.domain;

import java.util.List;

public interface GetScoreMungers {
	List<ScoreMunger> get();
}
