package smoslt.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

public class ProjectProfile implements Serializable {
	private static final long serialVersionUID = 3124008540434880323L;
	private Map<String, ProjectFactor> projectFactors;
	private SortedMap<String, String> categories;

	public ProjectProfile() {

	}

	public SortedMap<String, String> getCategories() {
		return categories;
	}

	public void setCategories(SortedMap<String, String> categories) {
		this.categories = categories;
	}

	public Map<String, ProjectFactor> getProjectFactors() {
		return projectFactors;
	}

	public void setProjectFactors(Map<String, ProjectFactor> projectFactors) {
		this.projectFactors = projectFactors;
	}

}
